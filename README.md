# HTTP Drills

## Aim

Write a program which will start a static server, and which would satisfy the following requests:

1. GET /html - Should return the HTML content.
2. GET /json - Should return the JSON string.
3. GET /uuid - Should return a UUID4.
4. GET /status/{status_code} - Should return a response with a status code as specified in the request
5. GET /delay/{delay_in_seconds} - Should return a success response but after the specified delay in the request.

# Run the program

- To start the local server, run the command:
`python3 server.py`

- To see the output webpage open any browser and visit:
1. [GET /html](http://localhost:8000/html)
2. [GET /json](http://localhost:8000/json)
3. [GET /uuid](http://localhost:8000/uuid)
4. [GET /status/{status_code}](http://localhost:8000/status/200)
5. [GET /delay/{delay_in_seconds}](http://localhost:8000/delay/2)