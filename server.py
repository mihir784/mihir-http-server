from http.server import HTTPServer, BaseHTTPRequestHandler
import uuid
import time


class requestHandler(BaseHTTPRequestHandler):
    def do_GET(self):

        if (self.path == '/html'):
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()
            output = open("./htmlCode.html").read()
            self.wfile.write(output.encode())

        if (self.path == '/json'):
            self.send_response(200)
            self.send_header('content-type', 'application/json')
            self.end_headers()
            output = open("./jsonObject.json").read()
            self.wfile.write(output.encode())

        if (self.path == '/uuid'):
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()
            output = "<head><title>UUID</title></head>" + \
                '{\n "uuid" : "' + str(uuid.uuid4()) + '" \n}'
            self.wfile.write(output.encode())

        if self.path.startswith('/status'):
            try:
                status_code = int(self.path.split("/")[-1])
                if status_code >= 200:

                    self.send_response(status_code)
                    self.send_header('content-type', 'text/plain')
                    self.end_headers()

                    if status_code < 300:
                        output = "Shows Success response"

                    elif status_code < 400:
                        output = "Shows Redirection response"

                    elif status_code < 500:
                        output = "Shows Client Errors response"

                    elif status_code < 600:
                        output = "Shows Server Errors response"

                    self.wfile.write(output.encode())

                elif status_code >= 100:
                    self.send_response(100)
                    self.flush_headers()

            except Exception:
                self.send_response(200)
                self.send_header('content-type', 'text/plain')
                self.end_headers()
                output = "Invalid Status Code"
                self.wfile.write(output.encode())

        if (self.path.startswith('/delay/')):
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()
            try:
                delay = int(self.path.split("/")[-1])
                if delay > 10:
                    delay = 10

                time.sleep(delay)
                output = "<head><title>Delayed Response</title></head>" + \
                    "<h2>Delayed Response by " + str(delay) + " seconds</h2>"
                self.wfile.write(output.encode())

            except Exception:
                output = "Invalid Time"
                self.wfile.write(output.encode())


def serveGetCall():
    PORT = 8000
    server_address = ('localhost', PORT)
    server = HTTPServer(server_address, requestHandler)
    print('Server running on port %s' % PORT)
    server.serve_forever()


serveGetCall()
